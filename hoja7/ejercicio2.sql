﻿DROP DATABASE IF EXISTS soldados;

CREATE DATABASE soldados;

USE soldados;

CREATE OR REPLACE TABLE compania(
  /* defino campos */
  num_compania int AUTO_INCREMENT,
  actividad varchar(200),
  /* restricciones */
  PRIMARY KEY(num_compania) -- clave principal
);


CREATE OR REPLACE TABLE soldado(
  cod_soldado int AUTO_INCREMENT,
  grado varchar(10),
  nombre varchar(50),
  apellidos varchar(100),
  PRIMARY KEY(cod_soldado)
);

CREATE OR REPLACE TABLE servicio(
  cod_servicio int AUTO_INCREMENT,
  descripcion varchar(100),
  PRIMARY KEY(cod_servicio)
);

CREATE OR REPLACE TABLE cuerpo(
  cod_cuerpo int AUTO_INCREMENT,
  denom varchar(50),
  PRIMARY KEY(cod_cuerpo)
);

CREATE OR REPLACE TABLE cuartel(
  cod_cuartel int AUTO_INCREMENT,
  nombre varchar(50),
  dir varchar(100),
  PRIMARY KEY(cod_cuartel)
);

CREATE OR REPLACE TABLE perteneceSoldado(
  num_compania int,
  cod_soldado int,
  PRIMARY KEY(num_compania,cod_soldado), 
  CONSTRAINT fk_pertenecesoldado_soldado FOREIGN KEY(cod_soldado) REFERENCES soldado(cod_soldado),
  CONSTRAINT fk_pertenecesoldado_compania FOREIGN KEY(num_compania) REFERENCES compania(num_compania)
);

CREATE OR REPLACE TABLE perteneceCuerpo(
  cod_soldado int,
  cod_cuerpo int,
  PRIMARY KEY(cod_soldado,cod_cuerpo),
  CONSTRAINT fk_pertenececuerpo_soldado FOREIGN KEY (cod_soldado) REFERENCES soldado(cod_soldado),
  CONSTRAINT fk_pertenececuerpo_cuerpo  FOREIGN KEY (cod_cuerpo) REFERENCES cuerpo(cod_cuerpo)
);

CREATE OR REPLACE TABLE esta(
cod_soldado int,
cod_cuartel int,
PRIMARY KEY(cod_soldado,cod_cuartel),
CONSTRAINT fk_esta_soldado FOREIGN KEY (cod_soldado) REFERENCES soldado(cod_soldado),
CONSTRAINT fk_esta_cuartel FOREIGN KEY(cod_cuartel) REFERENCES cuartel(cod_cuartel)
);

CREATE OR REPLACE TABLE realiza(
cod_soldado int,
cod_servicio int,
PRIMARY KEY(cod_soldado,cod_servicio),
CONSTRAINT fk_realiza_soldado FOREIGN KEY (cod_soldado) REFERENCES soldado(cod_soldado),
CONSTRAINT fk_realiza_servicio FOREIGN KEY (cod_servicio) REFERENCES servicio(cod_servicio)
);

CREATE OR REPLACE TABLE realizafecha(
cod_soldado int,
cod_servicio int,
fecha datetime,
PRIMARY KEY(cod_soldado,cod_servicio,fecha),
CONSTRAINT fk_realizafecha_realiza FOREIGN KEY(cod_soldado,cod_servicio) REFERENCES realiza(cod_soldado,cod_servicio)
);