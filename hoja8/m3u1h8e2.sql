﻿DROP DATABASE IF EXISTS m3u1h8e2;

CREATE DATABASE m3u1h8e2;

USE m3u1h8e2;

-- ZONA URBANA

CREATE OR REPLACE TABLE zonaurbana(
  nombrezona varchar(200),
  categoria varchar(100),
  PRIMARY KEY (nombrezona)
);

-- CASA PARTICULAR

CREATE OR REPLACE TABLE casaparticular(
  idcasa int AUTO_INCREMENT,
  numero varchar(4),
  nombrezona varchar(200),
  m2 int(11), -- usa 11 bits para el entero m2 float(6,2) sería un número de 6 dígitos (la coma también come un espacio) y dos decimales
  PRIMARY KEY(idcasa),
  UNIQUE (numero, nombrezona),
  CONSTRAINT fk_casaparticular_zonaurbana FOREIGN KEY (nombrezona) REFERENCES zonaurbana(nombrezona)
);

-- BLOQUE CASAS

CREATE OR REPLACE TABLE bloquecasas(
  idd int AUTO_INCREMENT,
  calle varchar(50),
  numero varchar(4),
  npisos int,
  nombrezona varchar(200),
  PRIMARY KEY (idd),
  UNIQUE (calle, numero),
  CONSTRAINT fk_bloquecasas_zonaurbana FOREIGN KEY (nombrezona) REFERENCES zonaurbana(nombrezona)
);

-- PISO

CREATE OR REPLACE TABLE piso (
  idP_P int AUTO_INCREMENT,
  planta varchar(3),
  puerta varchar(3),
  idd int,
  M2 int,
  PRIMARY KEY (idP_P),
  UNIQUE (planta, puerta, idd),
  CONSTRAINT fk_piso_bloquecasas FOREIGN KEY (idd) REFERENCES bloquecasas(idd)
);

-- PERSONA

CREATE OR REPLACE TABLE persona (
  dni varchar(9),
  nombre varchar(50),
  edad int,
  idpiso int,
  idcasa int,
  PRIMARY KEY (dni),
  CONSTRAINT fk_persona_piso FOREIGN KEY (idpiso) REFERENCES piso(idP_P),
  CONSTRAINT fk_persona_casaparticular FOREIGN KEY (idcasa) REFERENCES casaparticular(idcasa)
);

-- POSEE CASA

CREATE OR REPLACE TABLE poseecasa(
  idcasa int,
  dni varchar(9),
  PRIMARY KEY (idcasa, dni),
  CONSTRAINT fk_poseecasa_casaparticular FOREIGN KEY (idcasa) REFERENCES casaparticular(idcasa),
  CONSTRAINT fk_poseecasa_persona FOREIGN KEY (dni) REFERENCES persona(dni)
);

-- POSEE PISO

CREATE OR REPLACE TABLE poseepiso(
  idpiso int,
  dni varchar(9),
  PRIMARY KEY (idpiso,dni),
  CONSTRAINT fk_poseepiso_piso FOREIGN KEY (idpiso) REFERENCES piso(idP_P),
  CONSTRAINT fk_poseepiso_persona FOREIGN KEY (dni) REFERENCES persona(dni)
);

/*
   
 -- LAS FOREIGN KEY SE CREAN AL FINAL
  
 ALTER TABLE poseepiso
  ADD CONSTRAINT fk_poseepiso_piso 
  FOREIGN KEY (idpiso) 
  REFERENCES piso(idP_P),
  
  ADD CONSTRAINT fk_poseepiso_persona 
  FOREIGN KEY (dni) 
  REFERENCES persona(dni);
  
  
 */