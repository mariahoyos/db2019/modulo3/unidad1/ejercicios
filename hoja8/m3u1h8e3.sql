﻿DROP DATABASE IF EXISTS m3u1h8e3;

CREATE DATABASE m3u1h8e3;

USE m3u1h8e3;

CREATE OR REPLACE TABLE coleccion(
  codigo int AUTO_INCREMENT,
  precioestimado int,
  ubicacion varchar(100),
  PRIMARY KEY(codigo)
);

CREATE OR REPLACE TABLE persona(
  dni varchar(9),
  nombre varchar(50),
  direccion varchar(100),
  tfno varchar(9),
  codigocoleccion int,
  esprincipal bool,
  PRIMARY KEY(dni)
);

CREATE OR REPLACE TABLE especie(
  nombre varchar(50),
  caracteristicas varchar(200),
  PRIMARY KEY(nombre)
);

CREATE OR REPLACE TABLE mariposa(
  nombrecientifico varchar(50),
  origen varchar(50),
  habitat varchar(50),
  esperanzavida int,
  nombreespecie varchar(50),
  PRIMARY KEY(nombrecientifico)
);

CREATE OR REPLACE TABLE mariposacolores(
  nombrecientificomariposa varchar(50),
  colores varchar(100),
  PRIMARY KEY(nombrecientificomariposa,colores)
);

CREATE OR REPLACE TABLE ejemplar(
  nombrecientifico varchar(50),
  numero int,
  procedencia varchar(50),
  tamano int,
  codigocoleccion int,
  PRIMARY KEY(nombrecientifico,numero)
);

CREATE OR REPLACE TABLE coleccionespecie(
  codigocoleccion int,
  nombreespecie varchar(50),
  mejorejemplar varchar(100),
  PRIMARY KEY(codigocoleccion,nombreespecie)
);

ALTER TABLE persona
  ADD CONSTRAINT fk_persona_coleccion
  FOREIGN KEY(codigocoleccion)
  REFERENCES coleccion(codigo);

ALTER TABLE mariposa
  ADD CONSTRAINT fk_mariposa_especie
  FOREIGN KEY(nombreespecie)
  REFERENCES especie(nombre);

ALTER TABLE ejemplar
  ADD CONSTRAINT fk_ejemplar_coleccion
  FOREIGN KEY(codigocoleccion)
  REFERENCES coleccion(codigo),

  ADD CONSTRAINT fk_ejemplar_mariposa
  FOREIGN KEY(nombrecientifico)
  REFERENCES mariposa(nombrecientifico);

ALTER TABLE mariposacolores
  ADD CONSTRAINT fk_mariposacolores_mariposa
  FOREIGN KEY(nombrecientificomariposa)
  REFERENCES mariposa(nombrecientifico);

ALTER TABLE coleccionespecie
  ADD CONSTRAINT fk_coleccionespecie_codigocoleccion
  FOREIGN KEY(codigocoleccion)
  REFERENCES coleccion(codigo),

  ADD CONSTRAINT fk_coleccionespecie_especie
  FOREIGN KEY(nombreespecie)
  REFERENCES especie(nombre);


